export interface Extension {
    addon_key: string;
    key: string;
    url: string;
    options?: any;
}

export interface IframedModule {
    id: string;
    name: string;
    src: string;
}

export interface TargetSpec {
    addon_key?: string;
    key?: string;
    [x: string]: any;
}

export interface Module {
    [x: string]: any;
}

interface Host {
    /**
     * Send a message to iframes matching the targetSpec. This message is added to
     *  a message queue for delivery to ensure the message is received if an iframe
     *  has not yet loaded
     *
     * @param type The name of the event type
     * @param targetSpec The spec to match against extensions when sending this event
     * @param event The event payload
     * @param callback A callback to be executed when the remote iframe calls its callback
     */
    dispatch(type: string, targetSpec: TargetSpec, event: any, callback?: Function): any;

    /**
     * Send a message to iframes matching the targetSpec immediately. This message will
     *  only be sent to iframes that are already open, and will not be delivered if none
     *  are currently open.
     *
     * @param type The name of the event type
     * @param targetSpec The spec to match against extensions when sending this event
     * @param event The event payload
     */
    broadcast(type: string, targetSpec: TargetSpec, event: any): any;

    /**
     * Creates a new iframed module, without actually creating the DOM element.
     * The iframe attributes are passed to the 'setupCallback', which is responsible for creating
     * the DOM element and returning the window reference.
     *
     * @param extension The extension definition. Example:
     *   {
     *     addon_key: 'my-addon',
     *     key: 'my-module',
     *     url: 'https://example.com/my-module',
     *     options: { autoresize: false }
     *   }
     *
     * @param initCallback The optional initCallback is called when the bridge between host and iframe is established.
     */
    create(extension: Extension, initCallback?: Function): IframedModule;
    registerRequestNotifier(callback: Function): void;
    registerExtension(extension: Extension, initCallback: Function, unloadCallback: Function): string;
    registerKeyListener(extension_id: string, key: string, modifiers: any, callback: Function): void;
    unregisterKeyListener(extension_id: string, key: string, modifiers: any, callback: Function): void;
    defineModule(moduleName: string, module: Module): void;
    defineGlobals(module: Module): void;
    getExtensions(filter: any): any;
    unregisterExtension(filter: any): any;
    returnsPromise(wrappedMethod: Function): void;
}

declare var host: Host;

export default host;
