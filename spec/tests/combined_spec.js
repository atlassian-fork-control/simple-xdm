// tests combined.js specific things


import Combined from "combined/combined";

describe("combined host and plugin js tests", () => {
  var origin = window.location.origin;
  if (!origin) {
    origin = window.location.protocol + "//" + window.location.hostname + ( window.location.port ? ':' + window.location.port : '' );
  }
  var instance;
  var pluginData = {
    origin: origin,
    hostOrigin: origin,
    api: {
      some: {}
    }
  };

  beforeEach(() => {
    window.name = JSON.stringify(pluginData);
    instance = new Combined();
  });

  describe("Plugin side:", () => {
    it("AP.register is present", function(){
      expect(instance.register).toEqual(jasmine.any(Function));
    });

    it("AP.registerAny is present", function(){
      expect(instance.registerAny).toEqual(jasmine.any(Function));
    });

    it("plugin modules are present", function(){
      expect(instance.some).toEqual(pluginData.api.some);
    });
  });

  describe("host side: ", () => {
    it("defineGlobal is present", function(){
      expect(instance.defineGlobal).toEqual(jasmine.any(Function));
    });

    it("defineModule is present", function(){
      expect(instance.defineModule).toEqual(jasmine.any(Function));
    });

    it("subCreate is present", function(){
      expect(instance.subCreate).toEqual(jasmine.any(Function));
    });

  });

});
